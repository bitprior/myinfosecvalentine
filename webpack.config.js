var HTMLWebpackPlugin = require('html-webpack-plugin');

var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({

    template: __dirname + '/public/index.html',
    filename: 'index.html',
    inject: 'body'

})

module.exports = {
    entry: __dirname + '/src/index.js',
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    },
    devServer: {
        historyApiFallback:true
    },
    output: {
        filename: 'bundle.js',
        path: __dirname + '/build',
        publicPath: '/'
    },
    plugins: [HTMLWebpackPluginConfig]
};
