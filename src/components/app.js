import React, { Component } from 'react';
import { connect } from 'redux';
import {Typography, Card, CardContent} from 'material-ui';
import { Link } from 'react-router-dom';

class App extends Component{


    render(){

        return (
            <Card>
              <CardContent>
                <Typography variant='display1' >
                  Nice, the menu works.
                  Can the 'New Valentine' button go anywhere?
                </Typography>
              </CardContent>
            </Card>
        )

    }

}


export default App
