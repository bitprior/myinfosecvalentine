import React, {Component} from 'react';
import { Link } from 'react-router-dom'

class ValentineNew extends Component {
  render(){
    return(
      <div>
        This will be the new valentine page.
        You can also go back to the <Link to='/'>Homepage</Link>
      </div>
    )
  }
}

export default ValentineNew
