import React, { Component } from 'react';
import { Drawer,ListItemIcon, List, ListItem, ListItemText, Divider, ListSubheader, Paper } from 'material-ui';
import { toggleNav } from '../actions';
import {connect} from 'react-redux';
import { NavLink } from 'react-router-dom';

class NavDrawer extends Component {

    render() {

        const { navControl: navState } = this.props

        return (
        <Drawer anchor='left' open={navState} onClose={()=>{
          toggleNav()
        }} >
            <div style={{width:250}}>
                <List>
                    <ListSubheader>InfoSec Valentines</ListSubheader>
                    <NavLink to='/valentines/new' style={{textDecoration:'none'}}>
                    <ListItem button >

                        <ListItemText primary="New Valentine" />

                    </ListItem>
                        </NavLink>
                    <Divider />
                    <ListSubheader>Your Valentines</ListSubheader>
                    <ListItem>
                        <ListItemText secondary="You haven't made any valentines">
                        </ListItemText>
                    </ListItem>
                </List>
            </div>
        </Drawer>
        );

    }

}

const mapStateToProps = state => {
  return {
    navControl: state.navControl
  }
}

export default connect(
mapStateToProps
)(NavDrawer)
