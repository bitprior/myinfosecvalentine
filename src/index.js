import React, {Component} from 'react';
import ReactDom from 'react-dom';
import {MuiThemeProvider, createMuiTheme} from 'material-ui/styles';
import {AppBar, Toolbar, IconButton, Typography, Grid} from 'material-ui';
import MenuIcon from 'material-ui-icons/Menu';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import createHistory  from 'history/createBrowserHistory'
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';
import { createLogger }  from 'redux-logger';
import * as actions from './actions';
import { assignAll } from 'redux-act';
import {ConnectedRouter, routerReducer, routerMiddleware} from 'react-router-redux';

import App from './components/app';
import Mystery from './components/mystery';
import NavDrawer from './components/navDrawer';
import ValentineNew from './components/valentineNew';

import * as reducers from './reducers';

import {simpleSaga} from './sagas';

const history = createHistory()

const theme = createMuiTheme();

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  combineReducers({ ...reducers, routing:routerReducer}),
  applyMiddleware(routerMiddleware(history), createLogger())
)

assignAll(actions, store);
// sagaMiddleware.run(simpleSaga)

ReactDom.render(

  <Provider store={store}>

      <ConnectedRouter history={history}>
    <MuiThemeProvider theme={theme}>
      <div style={{
        width: '100%'
      }}>
        <AppBar position="static">
          <Toolbar>
            <IconButton color="inherit" aria-label="Menu" onClick={()=>{
              actions.toggleNav();
            }}>
              <MenuIcon/>
            </IconButton>
            <Typography variant="title" color="inherit">
              InfoSec Valentines
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
      <div>
          <NavDrawer />

          <Switch>
            <Route path='/valentines/new' component={ValentineNew} />
            <Route path='/secrets' component={Mystery}/>
            <Route path='/' component={App}/>
          </Switch>

        </div>

    </MuiThemeProvider>

          </ConnectedRouter>
  </Provider>, document.getElementById("root"));
