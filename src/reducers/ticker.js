import { createReducer } from 'redux-act';
import { ticker } from '../actions';

export const tickerReducer = createReducer({
    [ticker]: (state, payload) => state+payload
}, 0);

