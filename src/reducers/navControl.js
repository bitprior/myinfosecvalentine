import { createReducer } from 'redux-act';
import { toggleNav } from '../actions';

export const navControl = createReducer({
    [toggleNav]: (state) => !state
}, false);
