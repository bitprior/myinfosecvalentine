import { createAction } from 'redux-act';

export const ticker = createAction('Simple numerical ticker');
export const toggleNav = createAction('Toggle the nav drawer');
